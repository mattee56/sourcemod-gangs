#if defined _gangs_included_
  #endinput
#endif
#define _gangs_included_

#define s_ChatTag "\x01\x0B\x01[\x04Gangs\x01] "
#define MAX_GANGNAME_LENGTH 35

/*
* Checks if the core plugin of Gangs is running, and is connected to the database.
*/
native bool:Gangs_IsRunning();

/*
* Checks if a player is loaded/logged in.
*@param client Client index to check.
*@return True if player is loaded, false if not. (will also return false if player isn't connected)
*/
native bool:Gangs_IsPlayerLoaded(client);

/*
* Registers an upgrade, unless it already exists.
*@param UpgradeName The full name of the upgrade.
*@param UpgradeShortName The short name of the upgrade. (NEEDS TO BE UNIQUE)
*@param UpgradeMaxLevel The max level of the upgrade.
*@param UpgradeBasePrice The base price of the upgrade. (automatically multiplies itself per level) ~ might be changed
*@param UpgradeRepRequired The reputation required to buy the upgrade.
*/
native Gangs_RegisterUpgrade(String:UpgradeName[MAX_GANGNAME_LENGTH], String:UpgradeShortName[25], UpgradeMaxLevel, UpgradeBasePrice, UpgradeRepRequired);

/*
* Gets the clients played time.
*@param client Client to get played time of.
*@return Returns the amount of time a player has spent alive. (in seconds) Will always return 0 or higher. Even if a player isn't connected, so make sure they are.
*/
native Gangs_GetClientPlayedTime(client);

/*
* Logs a message for a gang.
*@param GangID ID of the gang
*@param Message String with the message to log.
*@return Returns true if it was able to log it, false if not.
* NOTE: ESCAPE YOUR SHIT
*/
native bool:Gangs_LogGang(GangID, String:sMessage[]);

/*
* Gets the upgrade level of a clients gang by the upgrade short name.
*
*@param client	Client index
*@param UpgradeShortName	Upgrades short name (unique identifier)
*@return Returns 0+ if:
* Gang upgrade is level 0 or higher.
*@return Returns -1 if:
* Client isn't in-game.
* Client isn't in a gang.
* Client is in a gang but gang couldn't be found.
* Upgrades unique identifier wasn't found.
*
*/
native Gangs_GetGangUpgradeLevelByClient(client, String:UpgradeShortName[25]);

/*
* Gets the upgrade level of a clients gang by the upgrade short name.
*
*@param GangID	Gang ID
*@param UpgradeShortName	Upgrades short name (unique identifier)
*@return Returns 0+ if:
* Gang upgrade is level 0 or higher.
*@return Returns -1 if:
* Invalid gang ID. (0 or less)
* Upgrades unique identifier wasn't found.
* Upgrades shortname passed is less than 1 character.
*/
native Gangs_GetGangUpgradeLevel(GangID, String:UpgradeShortName[25]);

/*
* Gets the clients gang.
*
*@param client Client index
*@return Returns the clients gang ID (1+)
*@return Returns 0 if:
* Client is not connected.
* Client is not in a gang.
*/
native Gangs_GetClientGang(client);

/*
* Gets the amount of money a gang has.
*
*@param GangID Gang ID
*@return Returns the amount of money a gang has.
*@return Returns -1 if:
* Gang does not exist.
* Invalid gang ID. (0 or less)
*/
native Gangs_GetGangMoney(GangID);

/*
* Gets the amount of money a gang has.
*
*@param GangID Gang ID
*@param Amount	Sets a gangs money
*@return Returns true if successful, false if not.
*/
native bool:Gangs_SetGangMoney(GangID, amount);

/*
* Gets the amount of reputation a gang has.
*
*@param GangID Gang ID
*@return Returns the amount of reputation a gang has.
*@return Returns -1 if:
* Gang does not exist.
* Invalid gang ID. (0 or less)
*/
native Gangs_GetGangRep(GangID);

/*
* Gets the amount of money a gang has.
*
*@param GangID Gang ID
*@param Amount	Sets a gangs money
*@return Returns true if successful, false if not.
*/
native bool:Gangs_SetGangRep(GangID, amount);

/*
* Sets a players rep.
*
*@param client Client index
*@param amount Amount to set the rep to
*@return Returns true if successful, false if not.
*/
native bool:Gangs_SetPlayerRep(client, amount);

/*
* Gets a players rep.
*
*@param client Client index
*@return Returns the amount of reputation a gang has.
*@return Returns -1 if:
* Client doesn't exist in DB or something.. :D
* Invalid client.
*/
native Gangs_GetPlayerRep(client);

/*
* Gets a gangs maxslots.
*
*@param GangID The gang ID to get max gang slots from.
*@return Returns -1 if the slots couldn't be fetched, otherwise returns the amount of slots.
*
*/
native Gangs_GetGangSlots(GangID);

/*
* Sets a gangs maxslots.
*
*@param GangID The gang ID to set max gang slots to.
*@param Slots The amount of slots to set as max slots.
*@return Returns -1 if the slots couldn't be fetched, otherwise returns the amount of slots.
*
*/
native bool:Gangs_SetGangSlots(GangID, Slots);

/*
* Checks if a player is in a gang.
*
*@param client Client index
*@return Returns true if a client is in gang, returns false if client isn't in a gang or if client isn't connected.
*
*/
native bool:Gangs_IsClientInGang(client);

/*
* Checks if a player is a gang leader.
*
*@param client Client index
*@return Returns true if a client is a gangleader, returns false if client isn't a gangleader or if client isn't connected.
*
*/
native bool:Gangs_IsClientGangLeader(client);

/*
* Gets a gangs name.
*
*@param GangID		Gang ID
*@param GangName	Buffer to store the gangs name
*@param maxlen		Max length of the buffer
*@return Returns true if successful, returns false if not.
*@error GangID is invalid/Gang couldn't be loaded.
*
*/
native bool:Gangs_GetGangName(GangID, String:GangName[], maxlen);

/*
* Sends a message to a whole gang.
*@param GangID Gang ID
*@param Message Message to send
*@return Returns true if successful, returns false if not.
*
*/
native bool:Gangs_SendGangMessage(GangID, String:sMessage[]);

/*
* Called when a player buys an upgrade.
*
*@param UserID	Client userID that purchased the upgrade
*@param GangID	The gang (ID) the upgrade was purchased for
*@param UpgradeShort	The short name of the upgrade purchased
*
*/
forward Gangs_OnUpgradePurchased(UserID, GangID, String:UpgradeShort[]);

/*
* Called when the main plugin is *hopefully* connected to the database.
*/
forward Gangs_OnDatabaseConnected();