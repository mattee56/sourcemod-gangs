"Phrases"
{
	"Player Reputation"
	{
		"#format"	"{1:d},{2:s}"
		"en"	"Your reputation is {1}, {2}!"
	}
	"Player Not Found"
	{
		"en"	"That player could not be found, sorry!"
	}
	"Player Profile Not Found"
	{
		"en"	"The profile of that player could not be found!"
	}
	"No Players To Unban"
	{
		"en"	"There are no banned players to unban!"
	}
	"No Target Found"
	{
		"en"	"Could not find a valid target with that name/userid!"
	}
	"No Results For Gang Top"
	{
		"en"	"There are no gangs to display!"
	}
	"No Results For Player Top"
	{
		"en"	"There are no players to display!"
	}
	"Gang MOTD"
	{
		"#format"	"{1:s}"
		"en"	"Message of the day: {1}"
	}
	"Gang MOTD Too Short"
	{
		"#format"	"{1:d},{2:d}"
		"en"	"!setgmotd <motd> ({1}-{2} characters)"
	}
	"Time Played"
	{
		"#format"	"{1:s},{2:s}"
		"en"	"You have played for {1} {2}!"
	}
	"Not Enough Played Time To Create A Gang"
	{
		"#format"	"{1:s},{2:s},{3:s}"
		"en"	"You have played {1} out of {2} {3} required to create a gang!"
	}
	"Player Rep"
	{
		"#format"	"{1:s},{2:d}"
		"en"	"{1}'s reputation is {2}!"
	}
	"Not In A Gang"
	{
		"en"	"You're not in a gang!"
	}
	"Not In A Gang Nor Any Invites"
	{
		"en"	"You're not in a gang nor do you have any pending invites!"
	}
	"Already In A Gang"
	{
		"en"	"You're already in a gang!"
	}
	"Gang Name Taken"
	{
		"#format"	"{1:s}"
		"en"		"Sorry, the gang name {1} is already taken!"
	}
	"Not A Gang Leader"
	{
		"en"	"You're not a gang leader!"
	}
	"Need To Be Gang Leader"
	{
		"en"	"You need to be a gang leader in order to do that!"
	}
	"Has Pending Invite"
	{
		"#format"	"{1:s}"
		"en"	"{1} already has an pending invite!"
	}
	"No Players To Invite"
	{
		"en"	"There are no players to invite, either there's no players online, or everyone is already in a gang!"
	}
	"Invited To Gang"
	{
		"#format"	"{1:s}" // 1 - name of invited player
		"en"	"You have invited {1} to join your gang!"
	}
	"Got Invited To Gang"
	{
		"#format"	"{1:s},{2:s}"
		"en"	"{1} invited you to join their gang {2}! Type !gang|/gang to join!"
	}
	"No Pending Invite"
	{
		"en"	"You do not have any pending gang invites!"
	}
	"Accepted Gang Invite"
	{
		"#format"	"{1:s}"
		"en"	"You have accepted the invite to join {1}!"
	}
	"Declined Gang Invite"
	{
		"#format"	"{1:s}"
		"en"	"You have declined the invite from {1}!"
	}
	"Player Left Gang"
	{
		"#format"	"{1:s}"
		"en"	"You have left the gang {1}!"
	}
	"Gang Leader Updated MOTD"
	{
		"#format"	"{1:s}"
		"en"	"Gang leader {1} has updated the gangs message of the day! (!gmotd)"
	}
	"Gang No Invites To Cancel"
	{
		"en"	"Your gang have no invites to cancel!"
	}
	"Gang Cancel Invite Not Connected"
	{
		"en"	"You cannot cancel the invite as the player is no longer connected, and thus the invitation has been canceled!"
	}
	"Gang Cancel Invite Not Invited Anymore"
	{
		"en"	"You cannot cancel the invite as the player is no longer invited to your gang!"
	}
	"Gang Cancel Invite Cancelled"
	{
		"#format"	"{1:s}"
		"en"	"You have canceled the invite to {1}!"
	}
	"Gang Cancel Invite Cancelled Player"
	{
		"#format"	"{1:s},{2:s}"
		"en"	"The gang invitation from {1} has been cancelled by {2}!"
	}
	"Gang Disbanded"
	{
		"#format"	"{1:s}"
		"en"	"Your gang have been disbanded by {1}!"
	}
	"Player Disbanded Gang"
	{
		"en"	"You have disbanded your gang!"
	}
	"No Gang Members Online"
	{
		"en"	"There are no gang members online!"
	}
	"Gang Player Kicked"
	{
		"#format"	"{1:s},{2:s}"
		"en"	"{1} kicked {2} out of the gang!"
	}
	"Gang Invite Gang Full"
	{
		"en"	"The gang you accepted an invite from is full and thus you cannot join!"
	}
	"Gang Invite Too Many Members"
	{
		"#format"	"{1:d},{2:d},{3:d},{4:d}"
		"en"	"You have the maximum {1}/{2} members! ({3} members & {4} invites)"
	}
	"Gang Accepted Gang Invite"
	{
		"#format"	"{1:s}"
		"en"	"{1} has accepted the invite and joined the gang!"
	}
	"Gang Declined Gang Invite"
	{
		"#format"	"{1:s}"
		"en"	"{1} has declined the gang-invite!"
	}
	"Gang Player Left"
	{
		"#format"	"{1:s}"
		"en"	"{1} has left the gang!"
	}
	"Gang Force Disband"
	{
		"en"	"You are the only leader of the gang and thus you need to appoint a new leader or disband the gang in order to leave!"
	}
	"Gang No Members To Edit"
	{
		"en"	"There are no gang members to edit!"
	}
	"Gang Leader Appointed"
	{
		"#format"	"{1:s},{2:s}"
		"en"	"Gangleader {1} has appointed {2} to gangleader!"
	}
	"Gang Leader Demoted"
	{
		"#format"	"{1:s},{2:s}"
		"en"	"Gangleader {1} has demoted {2} to a member!"
	}
	"Gang Can't Afford"
	{
		"#format"	"{1:d},{2:d}"
		"en"		"Your gang can't afford the upgrade, it costs ${1} and your gang have ${2}!"
	}
	"Gang Purchased Upgrade"
	{
		"#format"	"{1:s},{2:d},{3:d}"
		"en"		"You have successfully upgraded {1} to level {2} for ${3}!"
	}
	"Gang Create Banned"
	{
		"en"	"You cannot create a gang as you have been banned from creating gangs!"
	}
	"Gang You Got Banned"
	{
		"#format"	"{1:s}"
		"en"	"You got banned from creating gangs by {1}!"
	}
	"Gang You Banned"
	{
		"#format"	"{1:s}"
		"en"	"You banned {1} from creating gangs!"
	}
	"Gang You Got Unbanned"
	{
		"#format"	"{1:s}"
		"en"	"You got unbanned by {1}, allowing you to create gangs again!"
	}
	"Gang Unbanned ID"
	{
		"#format"	"{1:d}"
		"en"	"You unbanned ID {1}, allowing them to create gangs again!"
	}
	"Gang Player Already Banned"
	{
		"#format"	"{1:s}"
		"en"	"{1} is already banned from creating gangs!"
	}
	"Gang Deleted"
	{
		"#format"	"{1:s}"
		"en"	"You have deleted the gang {1} successfully!"
	}
	"Gang Not Loaded"
	{
		"en"	"Your gang has not been loaded, something has gone wrong!"
	}
}