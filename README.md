# Sourcemod Gangs v1.0.3b

## Core Features (as of v1.0.3b)

- [x] Basic API
- [x] Gang creation
- [x] Gang managing (disband, kick, edit leadership)
- [x] Gang upgrades (with an API)
- [x] a lot more, TBA docs

## Core To-Do (v1.0.3b->v1.0.4)
- [ ] Change the core plugin to the new syntax
- [ ] Change the baserewards plugin to the new syntax
- [x] Change the API to the new syntax
- [x] Make the API .inc cleaner
- [ ] Change the chattag upgrade to the new syntax
- [ ] Change the clantag upgrade to the new syntax
- [ ] Re-write/edit the translations
- [x] Add Gangs_FindGangByName to the API
- [ ] Edit the API to use userid instead of client
- [ ] Merge _info with _core
- [x] GetClientAuthString -> GetClientAuthId
- [ ] Clean up core code
- [ ] Change the way the total rep of a gang is counted
- [ ] Add return in SQL callbacks.
- [ ] Add support to disable and remove upgrades
- [ ] Edit SQL tables to be more clean.

## _Baserewards Features
- [x] Basic money & reputation rewards
- [x] Happy hour with config

## Core API
* bool Gangs_IsRunning - Checks if the gangs core is running AND is connected to its database.
* bool Gangs_IsPlayerLoaded - Checks if a player is loaded by the gangs plugin. (gang, stats etc)
* rest TBA.

## Upgrade Ideas
* X% chance to receive a weapon/nade on spawn
* Prisoner models

## Changelog

TBA